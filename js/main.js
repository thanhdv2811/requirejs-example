/**
 * The first JS file to be loaded. Takes care of setting up all of the required paths.
 */

//Configuration RequiresJs
require.config({
    baseUrl: 'js',
    paths: {
        'jquery': [
        	'//code.jquery.com/jquery',
        	//If the CDN localtion fails, load from this location
        	'lib/jquery'
        ],
        'bootstrap': [
        	'//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min',
        	//If the CDN localtion fails, load from this location
        	'lib/bootstrap.min'
        ],
        'jquery-ui': 'lib/jquery-ui.min',
        'jquery-form': 'lib/jquery.form',
        'modernizr': 'lib/modernizr.min'
    },
    deps: ["jquery"],
    shim: {
        'bootstrap' : ['jquery']
    }
});

require(['script/home']);